import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observer } from 'rxjs';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../shared/services/authentication.service';
import { takeUntilDestroyed } from '../shared/utils';
import { AuthenticatedUser } from '../models/auth-user';

@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loading = false;
  public loginError: string;
  public loginForm: FormGroup;
  public submitted = false;

  private returnRoute: string;

  constructor(
    private _authService: AuthenticationService,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router
  ) {
    if (this._authService.isAuthenticated()) {
      this._router.navigate(['patients']);
    }
  }

  ngOnInit() {
    this.loginForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      tenantId: ['', Validators.required]
    });

    // get return url from route parameters or default to '/'
    this.returnRoute = this._route.snapshot.queryParams.returnUrl || '/';
  }

  ngOnDestroy() { }

  public get ctrls() {
    return this.loginForm.controls;
  }

  public onSubmit(formData) {
    const $this = this;
    const loginObserver: Observer<AuthenticatedUser> = {
      next: user => {
        $this._router.navigate([$this.returnRoute]);
      },
      error: err => {
        $this.loginError = err.error.description;
        $this.loading = false;
      },
      complete: null
    };

    this.submitted = true;
    this.loading = true;
    this.loginError = null;

    this._authService
      .login(formData.username, formData.password, formData.tenantId)
      .pipe(
        first(),
        takeUntilDestroyed(this)
      ).subscribe(loginObserver);
  }

}
