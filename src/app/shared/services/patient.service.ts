import { Injectable } from '@angular/core';
import { Observable, } from 'rxjs';

import { Patient } from 'src/app/models/patient';
import { ApiService } from './api.service';
import { Guid } from 'src/app/models/guid';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  constructor(private _api: ApiService) { }

  public static readonly patientSort = (a: Patient, b: Patient) => {
    let nameA: string;
    let nameB: string;

    nameA = a.lastName + a.firstName;
    nameB = b.lastName + b.firstName;

    return nameA.localeCompare(nameB);
  }

  public getAll(): Observable<Patient[]> {
    return this._api.get<Patient[]>('/v1/patient');
  }

  public get(patientId: Guid): Observable<Patient[]> {
    const path = `/v1/patient/${patientId}`;
    return this._api.get<Patient[]>(path);
  }
}
