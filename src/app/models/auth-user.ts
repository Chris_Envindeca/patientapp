import { Guid } from './guid';

export interface AuthenticatedUser {
  id: Guid;
  name: string;
  defaultTenant: number;
  isAdministrator: boolean;
  token: string;
}
