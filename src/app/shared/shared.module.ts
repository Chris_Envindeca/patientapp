import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { SearchResultPipe } from './pipes/search-result.pipe';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library as fontLibrary } from '@fortawesome/fontawesome-svg-core';
import {
  faCog,
  faColumns,
  faFileMedicalAlt,
  faGripVertical,
  faHome,
  faKey,
  faMobileAlt,
  faTasks,
  faUser,
  faUserMd,
  faUsers,
  faTimes,
  faHospital,
  faAddressBook,
  faPhone,
  faPhoneSlash,
  faPhoneVolume,
  faSearch
} from '@fortawesome/free-solid-svg-icons';
import {PatientComponent} from '../patients-list/patient-info/patient.component';
import {PatientsListComponent} from '../patients-list/patients-list.component';

fontLibrary.add(
  faCog,
  faColumns,
  faFileMedicalAlt,
  faGripVertical,
  faHome,
  faKey,
  faMobileAlt,
  faTasks,
  faUser,
  faUserMd,
  faUsers,
  faTimes,
  faHospital,
  faAddressBook,
  faPhone,
  faPhoneSlash,
  faPhoneVolume,
  faSearch
);

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    FontAwesomeModule,
  ],
  declarations: [
    LoginComponent,
    PatientComponent,
    PatientsListComponent,
    SearchResultPipe
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    NgbModule,
    SearchResultPipe
  ],
  providers: [],
})
export class SharedModule {}
