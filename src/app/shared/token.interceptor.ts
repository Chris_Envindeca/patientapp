import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HTTP_INTERCEPTORS
} from '@angular/common/http';
import { Injectable, ClassProvider } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthenticatedUser } from 'src/app/models/auth-user';
import { Constants } from './constants';
import { StorageService } from './services/storage.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private _storage: StorageService) {}

  public static ToProvider(): ClassProvider {
    return {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    };
  }

  public intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this._storage.get<AuthenticatedUser>(Constants.UserKey);

    if (user && user.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${user.token}`
        }
      });
    }

    return next.handle(request);
  }
}
