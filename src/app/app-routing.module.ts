import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './login/login.component';
import {PatientsListComponent} from './patients-list/patients-list.component';
import {PatientComponent} from './patients-list/patient-info/patient.component';
import {AuthGuard} from './shared/auth.guard';


const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'patients', component: PatientsListComponent, canActivate: [AuthGuard]},
  { path: 'patient', component: PatientComponent, canActivate: [AuthGuard]},
  { path: 'patient',
    children: [
      {
        path: '',
        component: PatientComponent,
        canActivate: [AuthGuard],
      },
      {
        path: ':id',
        component: PatientComponent,
        canActivate: [AuthGuard],
      }
    ],
  },
  // otherwise redirect to home
  { path: '**', redirectTo: 'patients' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
