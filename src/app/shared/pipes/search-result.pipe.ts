import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchResult'
})
export class SearchResultPipe implements PipeTransform {
  transform(value: string, args?: any): any {
    let regex: RegExp;

    if (!args) {
      return value;
    }
    regex = new RegExp('(' + this.escapeRegExp(args) + ')', 'gi');
    return value.replace(regex, '<strong>$1</strong>');
  }

  private escapeRegExp(value: string): string {
    return value.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
  }
}
