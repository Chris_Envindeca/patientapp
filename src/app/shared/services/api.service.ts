import { HttpClient, HttpParams, HttpHeaders, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';

class HttpOptions {
  observe?: 'body';
  params?: HttpParams;
  responseType?: 'json';
}

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private _apiUrl: string;

  constructor(private _http: HttpClient) {
    this._apiUrl = environment.apiUrl;
  }

  /**
   * Constructs an HTTP DELETE request at the specified path.
   * @param path The absolute path of the GET request.
   * @param parameters Optional: Query parameters to be added.
   *
   * @return an `Observable` with a value of `true` when an entry was successfully deleted; otherwise, `false`.
   */
  public delete(path: string): Observable<boolean> {
    const url = this.getUrl(path);

    return this._http.delete<boolean>(url, new HttpOptions());
  }

  /**
   * Constructs an HTTP GET request at the specified path along with optionally supplied parameters.
   * @param path The absolute path of the GET request.
   * @param parameters Optional: Query parameters to be added.
   *
   * @return an `Observable` of the body as type `T`.
   */
  public get<T>(path: string, parameters?: HttpParams): Observable<T> {
    const options = new HttpOptions();
    const url = this.getUrl(path);

    if (parameters) {
      options.params = parameters;
    }

    return this._http.get<T>(url, options);
  }

  /**
   * Constructs an HTTP PATCH request at the specified path, providing the specified object as the body data.
   * @param path The absolute path of the PATCH request.
   * @param body Data to be sent.
   *
   * @return an `Observable` containing the patched object as type `T`.
   */
  public patch<T>(path: string, body: any): Observable<T> {
    const url = this.getUrl(path);

    return this._http.patch<T>(url, body, new HttpOptions());
  }

  /**
   * Constructs an HTTP POST request at the specified path, providing the specified object as the body data.
   * @param path The absolute path of the POST request.
   * @param body Data to be sent.
   *
   * @return an `Observable` of the body as type `T`.
   */
  public post<T>(path: string, body: object | null): Observable<T> {
    const url = this.getUrl(path);

    return this._http.post<T>(url, body, new HttpOptions());
  }

  /**
   * Constructs an HTTP POST request at the specified path, providing the specified object as the body data.
   * @param path The absolute path of the POST request.
   * @param body Data to be sent.
   *
   * @return an `Observable` of the body as type `T`.
   */
  public postFileForm<T>(path: string, body: object | null): Observable<HttpEvent<T>> {
    const url = this.getUrl(path);

    return this._http.post<T>(url, body, { reportProgress: true, observe: "events", responseType: "json" });
  }

  /**
   * Constructs an HTTP PUT request at the specified path, providing the specified object as the body data.
   * @param path The absolute path of the PUT request.
   * @param body Data to be sent.
   *
   * @return an `Observable` of the body typed to `any`.
   */
  public put(path: string, body: object | null): Observable<any> {
    const url = this.getUrl(path);

    return this._http.put(url, body, new HttpOptions());
  }

  private getUrl(path: string, id?: string | number): string {
    return `${this._apiUrl}${path}`;
  }
}

export class ApiPaths {
  // tslint:disable-next-line: variable-name
  public static readonly RouteLogin: string = '/login';
}
