import { Injectable } from '@angular/core';
import { Constants } from '../constants';
import { AuthenticatedUser } from '../../models/auth-user';

interface CacheEntry<T> {
  expiration: number;
  value: T;
}

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  private static readonly DEFAULT_EXPIRATION: number = 3600000; // 1 hour
  private _storage: Storage;

  constructor() {
    this._storage = window.sessionStorage;
  }

  public get<T>(key: string): T {
    const result = JSON.parse(this._storage.getItem(key)) as CacheEntry<T>;

    if (result) {
      if (result.expiration > Date.now()) {
        return result.value;
      }
      this._storage.removeItem(key);
    }

    return null;
  }

  public clear(key?: string): void {
    if (key === undefined) {
      this._storage.clear();
      return;
    }
    this._storage.removeItem(key);
  }

  public set(
    key: string,
    value: any,
    expiration: number = StorageService.DEFAULT_EXPIRATION
  ): void {
    const entry: CacheEntry<any> = {
      expiration: Date.now() + expiration,
      value
    };

    this._storage[key] = JSON.stringify(entry);
  }

  public checkIfExpiringSoon(key: string, expirationThreshold): boolean {
    const result = JSON.parse(this._storage.getItem(key)) as CacheEntry<any>;

    if (result) {

      // If expires within the threshold, return true.
      if (result.expiration > Date.now() && result.expiration - Date.now() <= expirationThreshold) {
        return true;
      }

      // If not expiring within the threshold, return false.
      if (result.expiration > Date.now()) {
        return false;
      }
    }

    // If key does not exist or key already expired, return null.
    return null;
  }


  public extendLoginExpiration(expiration: number = StorageService.DEFAULT_EXPIRATION) {
    const userEntry = JSON.parse(this._storage.getItem(Constants.UserKey)) as CacheEntry<AuthenticatedUser>;

    const newExpiration = Date.now() + expiration;
    userEntry.expiration = newExpiration;

    this._storage[Constants.UserKey] = JSON.stringify(userEntry);
  }

}
