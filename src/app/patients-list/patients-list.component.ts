import { Component, OnInit } from '@angular/core';
import { Patient } from '../models/patient';
import { Utils} from '../shared/utils';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientService } from '../shared/services/patient.service';

@Component({
  selector: 'app-patients',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients-list.component.css']
})
export class PatientsListComponent implements OnInit {

  public noPatients: boolean;
  public patients: Patient[];
  public filteredList: Patient[];
  public searchTxt: string;
  private _searchFunction: () => any;


  constructor(
    private _route: ActivatedRoute,
    private _patientService: PatientService,
    private _router: Router
  ) { }

  ngOnInit() {
    this._patientService.getAll().subscribe(this.onPatients.bind(this));
  }

  public onPatients(response) {
    this.patients = Array.isArray(response.data) ? response.data : [];

    this.patients.sort(((a: any, b: any) => {
      if (a.lastName < b.lastName) {
        return -1;
      } else if (a.lastName > b.lastName) {
        return 1;
      } else {
        return 0;
      }
    }));

    this.filteredList = this.patients;
    this._searchFunction = Utils.debounce(this, this.filterList, 500);
    console.log(this.patients);
    if (this.patients.length < 1) {
      this.noPatients = true;
    }
  }

  public onSearchChanged(): void {
    this._searchFunction();
  }

  public onPatientSelected(patient: Patient): void {

    console.log(patient.id);
    this._router.navigate(['patient', patient.id]);
  }

//#region Private Methods
  private filterList(): void {
    let filtered: Patient[];

    // TODO: This whole method will need to be changed to an API call.
    if (!this.searchTxt) {
      this.filteredList = this.patients;
      return;
    }

    const isMatch = (s1: string, s2: string) => {
      if (!s1 || !s2) {
        return false;
      }
      s1 = s1.toLowerCase();
      s2 = s2.toLowerCase();
      return s1.indexOf(s2) >= 0;
    };

    filtered = [];

    for (const patient of this.patients) {
      if (isMatch(patient.auxiliaryId, this.searchTxt)) {
        filtered.push(patient);
        continue;
      }
      if (isMatch(patient.firstName, this.searchTxt)) {
        filtered.push(patient);
        continue;
      }
      if (isMatch(patient.lastName, this.searchTxt)) {
        filtered.push(patient);
      }
    }

    // Logger.log(filtered.length);

    this.filteredList = filtered;
  }

}
