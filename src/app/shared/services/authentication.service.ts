import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { AuthenticatedUser } from 'src/app/models/auth-user';
import { Constants } from '../constants';
import { ApiService } from './api.service';
import { StorageService } from './storage.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  public currentUser: Observable<AuthenticatedUser>;

  private _currentUserSubject: BehaviorSubject<AuthenticatedUser>;

  constructor(private _api: ApiService, private _storage: StorageService) {
    const authenticatedUser = _storage.get<AuthenticatedUser>(Constants.UserKey);

    this._currentUserSubject = new BehaviorSubject<AuthenticatedUser>(authenticatedUser);
    this.currentUser = this._currentUserSubject.asObservable();
  }

  public isAuthenticated(): boolean {
    return !!this._currentUserSubject.value;
  }

  public login(username: string, password: string, tenantId: number): Observable<AuthenticatedUser> {
    const $this = this;
    return this._api.post<AuthenticatedUser>('/v1/Authentication/clinician/authenticate', {username, password, tenantId }).pipe(
      map(result => {
        // login successful if there's a jwt token in the response
        if (result && result.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          $this._storage.set(Constants.UserKey, result);

        }

        $this._currentUserSubject.next(result);
        return result;
      })
    );
  }

  public logout() {
    // remove user from local storage to log user out
    this._storage.clear();
    this._currentUserSubject.next(null);
  }
}
