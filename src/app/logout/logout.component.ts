import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Observer} from 'rxjs';
import {AuthenticatedUser} from '../models/auth-user';
import {first} from 'rxjs/operators';
import {takeUntilDestroyed} from '../shared/utils';
import {AuthenticationService} from '../shared/services/authentication.service';
import {Constants} from '../shared/constants';
import {ActivatedRoute, Router} from '@angular/router';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {
  public logoutForm: FormGroup;
  public loading = false;
  public loginError: string;
  public submitted = false;

  constructor(
    private _authService: AuthenticationService,
    public activeModal: NgbActiveModal,
    private _formBuilder: FormBuilder,
    private _route: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this.logoutForm = this._formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      tenantId: ['', Validators.required]
    });
  }

  ngOnDestroy() { }

  public get ctrls() {
    return this.logoutForm.controls;
  }

  public onSubmit(formData) {
    const $this = this;
    const loginObserver: Observer<AuthenticatedUser> = {
      next: user => {
         this._authService.logout();
         this._router.navigate([Constants.RouteLogin]);
        const $modal = this.activeModal;
        $modal.close();
      },
      error: err => {
        $this.loginError = err.error.description;
        $this.loading = false;
      },
      complete: null
    };

    this.submitted = true;
    this.loading = true;
    this.loginError = null;

    this._authService
      .login(formData.username, formData.password, formData.tenantId)
      .pipe(
        first(),
        takeUntilDestroyed(this)
      ).subscribe(loginObserver);
  }

}
