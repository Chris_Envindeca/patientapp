// tslint:disable: variable-name
export class Constants {
  public static readonly RouteLogin: string = "/login";
  public static readonly CallsKey: string = "__CALLS__";
  public static readonly DirectCallsKey: string = "__DIRECT_CALLS__";
  public static readonly StatKey: string = "__STATS__";
  public static readonly CallStatsKey: string = "__CALL_STATS__";
  public static readonly TenantKey: string = "__TENANT__";
  public static readonly UserKey: string = "__USER__";
  public static readonly ReportParamKey: string = "__REPORT__PARAM__";
}
