import { Guid } from './guid';
import * as moment from 'moment';
import _date = moment.unitOfTime._date;

export interface Patient {
  id: Guid;
  auxiliaryId: string;
  firstName: string;
  lastName: string;
  tenantId: number;
  isActive: boolean;
  dob: _date;
}
