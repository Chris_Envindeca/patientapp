import { isBuffer } from 'util';
import { ReplaySubject, Observable, MonoTypeOperatorFunction } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export class Utils {
  private static offset: number = new Date().getTimezoneOffset();

  public static debounce(
    context: any,
    func: () => any,
    wait: number,
    immediate: boolean = false
  ): () => any {
    let args: IArguments;
    let timeout: any;

    // This is the function that is actually executed when
    // the DOM event is triggered.
    return function executedFunction() {
      // Store the context of this and any
      // parameters passed to executedFunction
      args = arguments;

      // The function to be called after
      // the debounce time has elapsed
      const later = () => {
        // null timeout to indicate the debounce ended
        timeout = null;

        // Call function now if you did not on the leading end
        if (!immediate) {
          func.apply(context, args);
        }
      };

      // Determine if you should call the function
      // on the leading or trail end
      const callNow = immediate && !timeout;

      // This will reset the waiting every function execution.
      // This is the step that prevents the function from
      // being executed because it will never reach the
      // inside of the previous setTimeout
      clearTimeout(timeout);

      // Restart the debounce waiting period.
      // setTimeout returns a truthy value (it differs in web vs node)
      timeout = setTimeout(later, wait);

      // Call immediately if you're doing a leading
      // end execution
      if (callNow) {
        func.apply(context, args);
      }
    };
  }

  public static getEnumName<T, TValue extends { [P in keyof T]: number }>(
    enumObj: T | any,
    status: TValue
  ): string {
    const name = enumObj[status];

    return name.replace(/([A-Z]+)*([A-Z][a-z])/g, '$1 $2');
  }

  public static toLocalDate(value: Date) {
    let date: Date;
    if (!value) {
      return null;
    }
    date = new Date(value);
    const minutes = date.getMinutes() - Utils.offset;
    date.setMinutes(minutes);
    return date;
  }

  public static deepEquals(a: any, b: any): boolean {
    // From: https://github.com/epoberezkin/fast-deep-equal/
    if (a === b) {
      return true;
    }

    if (a && b && Utils.isObject(a) && Utils.isObject(b)) {
      const isArray1 = Array.isArray(a);
      const isArray2 = Array.isArray(b);

      if (isArray1 !== isArray2) {
        return false;
      }

      if (isArray1 && isArray2) {
        const length1 = a.length;
        if (length1 !== b.length) {
          return false;
        }
        for (let i = length1; i-- !== 0; ) {
          if (!Utils.deepEquals(a[i], b[i])) {
            return false;
          }
        }
        return true;
      }

      const dateA = a instanceof Date;
      const dateB = b instanceof Date;

      if (dateA !== dateB) {
        return false;
      }
      if (dateA && dateB) {
        return a.getTime() === b.getTime();
      }

      const regexpA = a instanceof RegExp;
      const regexpB = b instanceof RegExp;

      if (regexpA !== regexpB) {
        return false;
      }
      if (regexpA && regexpB) {
        return a.toString() === b.toString();
      }

      const keys = Object.keys(a);
      const length = keys.length;

      if (length !== Object.keys(b).length) {
        return false;
      }

      const hasProp = Object.prototype.hasOwnProperty;
      for (let i = length; i-- !== 0; ) {
        if (!hasProp.call(b, keys[i])) {
          return false;
        }
      }

      for (let i = length; i-- !== 0; ) {
        const key = keys[i];
        if (!Utils.deepEquals(a[key], b[key])) {
          return false;
        }
      }

      return true;
    }

    return a !== a && b !== b;
  }

  public static isEmptyObject(value: any) {
    return Object.entries(value).length === 0 && value.constructor === Object;
  }

  private static isObject(value: any) {
    const s: string = Object.prototype.toString.call(value);
    return s === '[object Object]';
  }

  private static isEqual(a: any, b: any): boolean {
    let actualKeys: string[];
    let expectedKeys: string[];

    if (a.prototype !== b.prototype) {
      return false;
    }
    if (isBuffer(a)) {
      if (!isBuffer(b)) {
        return false;
      }
      if (a.length !== b.length) {
        return false;
      }
      for (let i = 0; i < a.length; i++) {
        if (a[i] !== b[i]) {
          return false;
        }
      }
      return true;
    }

    try {
      actualKeys = Object.keys(a);
      expectedKeys = Object.keys(b);
    } catch (e) {
      // Can occur when one is a string literal and the other isn't
      return false;
    }

    if (actualKeys.length !== expectedKeys.length) {
      return false;
    }

    actualKeys.sort();
    expectedKeys.sort();

    // Cheap key test
    for (let i = actualKeys.length - 1; i >= 0; i--) {
      if (actualKeys[i] !== expectedKeys[i]) {
        return false;
      }
    }

    for (let i = actualKeys.length - 1; i >= 0; i--) {
      const key = actualKeys[i];
      if (!Utils.deepEquals(a[key], b[key])) {
        return false;
      }
    }

    return typeof a === typeof b;
  }
}

function componentDestroyed(component: any) {
  const modifiedComponent = component;
  if (modifiedComponent.__componentDestroyed$) {
    return modifiedComponent.__componentDestroyed$;
  }

  const oldNgOnDestroy = component.ngOnDestroy;

  if (!oldNgOnDestroy) {
    throw new Error('ngOnDestroy() is required in components that use takeUntilDestroyed().');
  }

  const stopSubject$ = new ReplaySubject();

  modifiedComponent.ngOnDestroy = () => {
    oldNgOnDestroy.apply(component);

    stopSubject$.next(true);
    stopSubject$.complete();
  };

  modifiedComponent.__componentDestroyed$ = stopSubject$.asObservable();
  return modifiedComponent.__componentDestroyed$;
}

export function takeUntilDestroyed<T>(component: any): MonoTypeOperatorFunction<T> {
  return (source$: Observable<T>) => {
    return source$.pipe(takeUntil(componentDestroyed(component)));
  };
}
