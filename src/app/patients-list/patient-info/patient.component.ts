import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LogoutComponent} from '../../logout/logout.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PatientService} from '../../shared/services/patient.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  public name: string;
  public auxId: string;
  constructor(
    private _route: ActivatedRoute,
    private _patientService: PatientService,
    private _router: Router,
    private _modalService: NgbModal,
    ) { }

  ngOnInit() {
    // retrieves selected patientID
    this._route.params.subscribe(this.onPatient.bind(this));
  }

  // retrieves patients Info
  onPatient(response) {
    this._patientService.get(response.id).subscribe(this.onGetPatient.bind(this));
  }

  // manipulates patients data to displays
  onGetPatient(response) {
    this.name = response.firstName + ' ' + response.lastName;
    this.auxId = response.auxiliaryId;
  }

  public logout() {
    const modalRef = this._modalService.open(LogoutComponent, {
      size: 'md' as 'lg',
    });

    modalRef.result.then(
      () => {
        // $component.reloadResponders();
      },
      () => {
        return;
      });
  }

}
