import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';

import { Constants } from './constants';
import { AuthenticationService } from './services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private _router: Router, private _authenticationService: AuthenticationService) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    let allowActivation = true;

    if (!this._authenticationService.isAuthenticated()) {
      // not logged in so redirect to login page with the return url
      this._router.navigate([Constants.RouteLogin], { queryParams: { returnUrl: state.url } });
      allowActivation = false;
    }

    return allowActivation;
  }
}
